from django.contrib import admin
from django.urls import path, re_path, include
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='University Manager APIs')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('users/', include('users.urls', namespace='users')),
    
    
    # third party app urls
    path("__reload__/", include("django_browser_reload.urls")),
    re_path(r'^$', schema_view),
    path('schema/', include('schema_viewer.urls')),
    
    
    # apis urls
    path('api/users/', include('users.api.urls', namespace='users_apis')),
]


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)