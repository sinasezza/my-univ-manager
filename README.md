# University Manager Website Back-End 🏢

## admin panel 🔐:
<img src="readme/admin_panel.jpg">


## Features


## Required Software

Ensure you have the following software installed:

- [Python 3.10](https://www.python.org/downloads/) or newer
- [Node.js 18.15 LTS](https://nodejs.org/) or newer (required for ReactJs)
- [Git](https://git-scm.com/)


## Setup

## get the repository
```bash
git clone https://gitlab.com/sinasezza/my-univ-manager.git
cd my-univ-manager
```


### Configure Environment
_macOS/Linux Users_
```bash
python manage.py -m venv venv
source ./venv/bin/activate
pip install poetry
poetry install
```

_Windows Users_
```bash
python manage.py -m venv venv
venv\Scripts\activate
pip install poetry
poetry install
```


### Run The Django Http Server
```bash
poetry run python manage.py makemigrations
poetry run python manage.py migrate
poetry run python manage.py runserver
```