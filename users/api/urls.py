from django.urls import path, re_path
from . import apis


app_name = 'users_apis'


urlpatterns = [
    path('list/', apis.user_list_api, name='user-list'),
]
