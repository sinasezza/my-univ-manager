from rest_framework.request import HttpRequest
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework import authentication as rest_authentications
from rest_framework import permissions as rest_permissions
from rest_framework import authentication
from rest_framework.response import Response
from rest_framework import status as rest_statuses
from .. import models, serializers


@api_view(['GET'])
@authentication_classes([])
@permission_classes([rest_permissions.AllowAny])
def user_list_api(request: HttpRequest) -> Response:
    users = models.User.objects.all()
    response_data = serializers.UserSerializer(users, many=True).data
    return Response(data={'results': response_data}, status=rest_statuses.HTTP_200_OK)