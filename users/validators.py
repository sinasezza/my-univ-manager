import re
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy as _


phone_number_validator = RegexValidator(
    regex=r'^(\+?[0-9]{1,3})?[0-9]{10,12}$',
    message='لطفا یک شماره تلفن معتبر وارد کنید.'
)


def validate_ssn(ssn: str) -> None:
    """validate ssn for iranian national codes

    Args:
        ssn (str): the national code

    Raises:
        ValidationError: if length  of `ssn` is not equal to  10 or does not match formula
    Returns:
        None
    """
    if not re.search(r'^\d{10}$', ssn): 
        raise ValidationError (message='لطفا یک کد ملی معتبر وارد کنید.', params={'ssn': ssn,})

    # check = int(ssn[9])
    # s = sum(int(ssn[x]) * (10 - x) for x in range(9)) % 11
    # if not (check == s if s < 2 else check + s == 11) :
    #     raise ValidationError (message='لطفا یک کد ملی معتبر وارد کنید.', params={'ssn': ssn,})
