from django.db import models
from django.contrib.auth.models import PermissionsMixin, AbstractBaseUser
from django.core.validators import RegexValidator, EmailValidator, MinValueValidator, MaxValueValidator
from django.utils.translation import gettext_lazy as _
from django_jalali.db import models as jmodels
from . import managers, validators


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(
        max_length=20,
        unique=True,
        validators=[validators.validate_ssn],
        help_text="a numeric username with maximum length 20 characters",
        verbose_name='یوزرنیم/کد ملی'
    )

    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )

    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = jmodels.jDateTimeField(auto_now_add=True, verbose_name='تاریخ عضویت')
    last_login = jmodels.jDateTimeField(auto_now=True, blank=True, null=True, verbose_name='آخرین ورود')
    

    REQUIRED_FIELDS = []
    USERNAME_FIELD = 'username'
    objects = managers.CustomUserManager()

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = 'کاربر'
        verbose_name_plural = 'کاربران'



# ====================================================================================================


class Person(models.Model):
    user = models.ForeignKey(User, related_name='%(class)s', on_delete=models.CASCADE, verbose_name='یوزر')
    first_name = models.CharField(max_length=120, null=False, verbose_name='نام')
    last_name = models.CharField(max_length=120, null=False, verbose_name='نام خانوادگی')
    gender = models.BooleanField(verbose_name='جنسیت')
    ssn = models.CharField(max_length=10, unique=True, validators=[validators.validate_ssn], verbose_name='کد ملی')
    phone_number = models.CharField(max_length=13, validators=[validators.phone_number_validator], verbose_name='شماره تلفن')
    email = models.EmailField(max_length=80, validators=[EmailValidator()], blank=True, null=True, verbose_name='ایمیل')

    class Meta:
        abstract = True

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"
    
    @property
    def username(self):
        return self.user.username
    
    
# ====================================================================================================


class Professor(Person):
    number = models.CharField(max_length=20, unique=True, null=False, db_index=True, verbose_name='شماره استادی')
    orientation = models.ForeignKey(to='lessons.Orientation', on_delete=models.SET_NULL, blank=True, null=True, related_name='professors', verbose_name='گرایش')

    class Meta:
        verbose_name = 'استاد'
        verbose_name_plural = 'اساتید'
        
        
    def __str__(self):
        return f"{self.number} - {self.full_name} - {self.username}"
    


# ====================================================================================================


class Student(Person):
    number = models.CharField(max_length=11, unique=True, null=False, db_index=True, verbose_name='شماره دانشجویی')
    field_name = models.CharField(max_length=64, verbose_name='رشته تحصیلی')
    academic_orientation = models.CharField(max_length=64, verbose_name='گرایش')
    

    class Meta:
        verbose_name = 'دانشجو'
        verbose_name_plural = 'دانشجویان'

    def __str__(self):
        return f"{self.number} - {self.full_name} - {self.username}"


# ====================================================================================================


class Employee(Person):
    number = models.CharField(max_length=20, unique=True, null=False, db_index=True, verbose_name='شماره کارمندی')

    class Meta:
        verbose_name = 'کارمند'
        verbose_name_plural = 'کارمندان'

    def __str__(self):
        return f"{self.number} - {self.full_name} - {self.username}"


# ====================================================================================================

class News(models.Model):
    employee = models.ForeignKey(to=Employee, on_delete=models.CASCADE, related_name='news', verbose_name='کارمند')
    text = models.CharField(max_length=200, verbose_name='متن')
    date_created = jmodels.jDateTimeField(auto_now_add=True, verbose_name='تاریخ ایجاد')
    audience = models.PositiveIntegerField(blank=True, null=True, verbose_name='گیرندگان')
    
    class Meta:
        verbose_name = 'خبر'
        verbose_name_plural = 'اخبار'
        
        
# ====================================================================================================