from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from . import models


@admin.register(models.User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'is_active', 'is_staff', 'is_superuser', 'last_login',)


@admin.register(models.Professor)
class ProfessorAdmin(admin.ModelAdmin):
    list_display = ('user', 'full_name', 'gender',)


@admin.register(models.Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ('user', 'full_name', 'gender',)
    


@admin.register(models.Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('user', 'full_name', 'gender',)


@admin.register(models.News)
class NewsAdmin(admin.ModelAdmin):
    list_display = ('employee', 'text', 'date_created',)