from django.contrib import admin
from . import models


@admin.register(models.Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = ('number', 'title', 'unit',)


@admin.register(models.Semester)
class SemesterAdmin(admin.ModelAdmin):
    list_display = ('number', 'title', 'year',)
 

@admin.register(models.Week)
class WeekAdmin(admin.ModelAdmin):
    list_display = ('number', 'semester',)
    
 
@admin.register(models.PresentedCourse)
class PresentedCourseAdmin(admin.ModelAdmin):
    list_display = ('semester', 'course', 'group_number')
    

@admin.register(models.Present)
class PresentAdmin(admin.ModelAdmin):
    list_display = ('course', 'professor')


@admin.register(models.Take)
class TakeAdmin(admin.ModelAdmin):
    list_display = ('course', 'student',)
                    

@admin.register(models.Session)
class SessionAdmin(admin.ModelAdmin):
    list_display = ('number', 'day_number', 'start_time', 'end_time',)       
    

@admin.register(models.PresentedTime)
class PresentedTimeAdmin(admin.ModelAdmin):
    list_display = ('course', 'session', 'week',)              

