from django.db import models
from django.utils import timezone
from django_jalali.db import models as jmodels
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from users.models import Student, Professor


class Faculty(models.Model):
    number = models.CharField(max_length=12, verbose_name='شماره')
    name = models.CharField(max_length=120, verbose_name='نام')
    
    class Meta:
        verbose_name = 'دانشکده'
        verbose_name_plural = 'دانشکده ها'
        
    def __str__(self) -> str:
        return f"{self.number}  - {self.name}"


# ====================================================================================================


class Major(models.Model):
    faculty = models.ForeignKey(to=Faculty, on_delete=models.SET_NULL, blank=True, null=True, related_name='majors', verbose_name='دانشکده')
    number = models.CharField(max_length=12, verbose_name='شماره')
    name = models.CharField(max_length=120, verbose_name='نام')

    class Meta:
        verbose_name = 'رشته'
        verbose_name_plural = 'رشته ها'
        
    def __str__(self) -> str:
        return f"{self.faculty} - {self.number}  - {self.name}"

# ====================================================================================================


class Orientation(models.Model):
    major  = models.ForeignKey(Major, on_delete=models.CASCADE, related_name='orientations', verbose_name="رشته")
    number = models.CharField(max_length=12, verbose_name='شماره')
    name = models.CharField(max_length=120, verbose_name='نام')
    required_credits = models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='تعداد واحد مانده')

    class Meta:
        verbose_name = 'گرایش'
        verbose_name_plural = 'گرایش ها'
        
    def __str__(self) -> str:
        return f"{self.major} - {self.number}  - {self.name} - {self.required_credits}"


# ====================================================================================================


class Grade(models.Model):
    number = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(0), MaxValueValidator(4),],
        verbose_name='کد'
    )
    name = models.CharField(max_length=120, verbose_name='نام')

    class Meta:
        verbose_name = 'مقطع تحصیلی'
        verbose_name_plural = 'مقاطع تحصیلی'
        
    def __str__(self) -> str:
        return f"{self.number}-{self.name}"


# ====================================================================================================


class Study(models.Model):
    orientation = models.ForeignKey(to=Orientation, on_delete=models.PROTECT, related_name='studies', verbose_name='گرایش')
    student = models.OneToOneField(to=Student, on_delete=models.CASCADE, related_name='study', verbose_name='دانشجو')
    grade = models.ForeignKey(to=Grade, on_delete=models.SET_NULL, blank=True, null=True, related_name='studies', verbose_name='مقطع تحصیلی')
    start_date = jmodels.jDateField(blank=True, null=True, verbose_name='تاریخ شروع')
    end_date = jmodels.jDateField(blank=True, null=True, verbose_name='تاریخ پایان')
    
    class Meta:
        verbose_name = 'تحصیل'
        verbose_name_plural = 'تحصیلات'
        
    def __str__(self) -> str:
        return f"{self.student}"
    
    def clean(self) -> None:
        # Check if end_date is not less than start_date
        if self.end_date and self.start_date and self.end_date < self.start_date:
            raise ValidationError('تاریخ پایان نباید کمتر از تاریخ شروع باشد.')


# ====================================================================================================



class CourseType(models.Model):
    number = models.PositiveSmallIntegerField(
        db_index=True, 
        validators= [MinValueValidator(0), MaxValueValidator(10)],
        verbose_name='کد')
    name = models.CharField(max_length=60, verbose_name='نام')

    class Meta:
        verbose_name = 'نوع درس'
        verbose_name_plural = 'انواع درس'
        
    def __str__(self) -> str:
        return f"{self.number}-{self.name}"
    
# ====================================================================================================


class Course(models.Model):
    number = models.CharField(max_length=12, db_index=True, verbose_name='کد درس')
    title = models.CharField(max_length=120, null=False, verbose_name='نام درس')
    unit = models.PositiveSmallIntegerField(null=False, validators=[MinValueValidator(1), MaxValueValidator(6)], verbose_name='تعداد واحد')

    class Meta:
        verbose_name = 'درس'
        verbose_name_plural = 'دروس'
        
        
    def __str__(self):
        return f'{self.title} - {self.number} - {self.unit}'
    
    
# ====================================================================================================


class Have(models.Model):
    orientation = models.ForeignKey(to=Orientation, on_delete=models.PROTECT, related_name='haves', verbose_name='گرایش')
    course = models.ForeignKey(to=Course, on_delete=models.SET_NULL, blank=True, null=True, related_name='haves', verbose_name='درس')
    course_type = models.ForeignKey(to=CourseType, on_delete=models.SET_NULL, blank=True, null=True, related_name='haves', verbose_name='نوع درس')

    class Meta:
        verbose_name = 'داشتن درس'
        verbose_name_plural = 'داشتن دروس'
        
    def __str__(self) -> str:
        return f"{self.number}-{self.name}"

# ====================================================================================================


class Semester(models.Model):
    number = models.CharField(max_length=8, db_index=True, verbose_name='کد ترم')
    title = models.CharField(max_length=120, verbose_name='نام ترم')
    year = models.CharField(max_length=8, verbose_name='تاریخ')

    class Meta:
        verbose_name = 'ترم'
        verbose_name_plural = 'ترم ها'
        
        
    def __str__(self):
        return self.title


# ====================================================================================================


class Week(models.Model):
    number = models.PositiveSmallIntegerField(
        db_index=True, 
        verbose_name='کد هفته',
        validators=[
            MinValueValidator(1),
            MaxValueValidator(18)
        ]
    )
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE, related_name='weeks', verbose_name='ترم')
    start_date = jmodels.jDateField(verbose_name='تاریخ شروع')
    end_date = jmodels.jDateField(verbose_name='تاریخ پایان')

    class Meta:
        verbose_name = 'هفته'
        verbose_name_plural = 'هفته ها'
        unique_together = ('number', 'semester')

    def __str__(self):
        return f'{self.number} - {self.semester.title}'

    def clean(self) -> None:
        # Check if the week number is greater than 1
        if self.number > 1:
            previous_week_exists = Week.objects.filter(number=self.number - 1, semester=self.semester).exists()
            if not previous_week_exists:
                raise ValidationError('هفته قبلی وجود ندارد. لطفاً هفته قبلی را ابتدا ایجاد کنید.')

        # Check if end_date is not less than start_date
        if self.end_date and self.start_date and self.end_date < self.start_date:
            raise ValidationError('تاریخ پایان نباید کمتر از تاریخ شروع باشد.')


# ====================================================================================================


class PresentedCourse(models.Model):
    semester = models.ForeignKey(
        Semester,
        on_delete=models.CASCADE,
        related_name='presented_courses',
        verbose_name='ترم'
    )
    course = models.ForeignKey(
        Course,
        on_delete=models.CASCADE,
        related_name='presented_courses',
        verbose_name='درس'
    )
    group_number = models.PositiveSmallIntegerField(verbose_name='شماره گروه')

    class Meta:
        verbose_name = 'درس ارائه شده'
        verbose_name_plural = 'دروس ارائه شده'
        
        unique_together = ('semester', 'course', 'group_number')
        
        
    def __str__(self):
        return f"{self.semester.title} - {self.course.title} - {self.group_number}"


# ====================================================================================================


class Present(models.Model):
    course = models.ForeignKey(
        PresentedCourse,
        on_delete=models.CASCADE,
        related_name='presents',
        verbose_name='درس ارائه شده'
    )
    professor = models.ForeignKey(
        Professor,
        on_delete=models.CASCADE,
        related_name='presents',
        verbose_name='استاد'
    )

    class Meta:
        verbose_name = 'ارائه دادن درس'
        verbose_name_plural = 'ارائه دادن دروس'

        unique_together = ('course', 'professor',)
        
        
    def __str__(self):
        return f"{self.course} - {self.professor}"


# ====================================================================================================


class Take(models.Model):
    course = models.ForeignKey(
        PresentedCourse,
        on_delete=models.CASCADE,
        related_name='takes_enrollments',
        verbose_name='درس ارائه شده'
    )
    student = models.ForeignKey(
        Student,
        on_delete=models.CASCADE,
        related_name='takes_enrollments',
        verbose_name='دانشجو'
    )

    class Meta:
        verbose_name = 'اخذ کردن درس'
        verbose_name_plural = 'اخذ کردن دروس'

        unique_together = ('course', 'student',)
        
        
    def __str__(self):
        return f"{self.course} - {self.student}"


# ====================================================================================================


class Session(models.Model):
    number = models.PositiveSmallIntegerField(verbose_name='شماره جلسه')
    day_number = models.PositiveSmallIntegerField(
        verbose_name='شماره روز بین 0 تا 6',
        validators=[
            MinValueValidator(0),
            MaxValueValidator(6)
        ]
    )
    start_time = models.TimeField(verbose_name='ساعت شروع')
    end_time = models.TimeField(verbose_name='ساعت پایان')

    class Meta:
        verbose_name = 'جلسه'
        verbose_name_plural = 'جلسه ها'
        
        
    def __str__(self):
        return f"{self.number} - {self.day_number}"

    def clean(self):
        if self.start_time and self.end_time and self.start_time > self.end_time:
            raise ValidationError('ساعت شروع باید کوچکتر از ساعت پایان باشد')
            

# ====================================================================================================


class PresentedTime(models.Model):
    course = models.ForeignKey(
        PresentedCourse,
        on_delete=models.CASCADE,
        related_name='presented_times',
        verbose_name='درس'
    )

    session = models.ForeignKey(
        Session,
        on_delete=models.CASCADE,
        related_name='presented_times',
        verbose_name='جلسه'
    )

    week = models.ForeignKey(
        Week,
        on_delete=models.CASCADE,
        related_name='presented_times',
        verbose_name='هفته'
    )

    class Meta:
        verbose_name = 'زمان ارائه درس'
        verbose_name_plural = 'زمان ارائه دروس'

        unique_together = ('course', 'session', 'week')
        

    def __str__(self):
        return f"{self.course} - {self.session} - {self.week}"

