from django.db import models
from django_jalali.db import models as jmodels
from lessons.models import (
    Session, 
    Week, 
    PresentedCourse,
)
from users.models import (
    Student, 
    Professor,
)


class RollCallTimePosition(models.Model):
    number = models.PositiveSmallIntegerField(verbose_name='شماره')
    time = jmodels.jDateTimeField(auto_now_add=True, verbose_name='زمان ثبت')
    position = models.CharField(max_length=200, blank=True, null=True, verbose_name='مکان ثبت')

    class Meta:
        verbose_name = 'مکان/زمان ثبت'
        verbose_name_plural = 'مکان/زمان های ثبت'
        
    def __str__(self):
        return f"{self.number}-{self.time}"

# ====================================================================================================


class RollCall(models.Model):
    session = models.ForeignKey(
        Session,
        on_delete=models.CASCADE,
        related_name='rollcalls',
        verbose_name='جلسه'
    )
    week = models.ForeignKey(
        Week,
        on_delete=models.CASCADE,
        related_name='rollcalls',
        verbose_name='هفته'
    )
    student = models.ForeignKey(
        Student,
        on_delete=models.CASCADE,
        related_name='rollcalls',
        verbose_name='دانشجو'
    )
    professor = models.ForeignKey(
        Professor,
        on_delete=models.CASCADE,
        related_name='rollcalls',
        verbose_name='استاد'
    )
    presented_course = models.ForeignKey(
        PresentedCourse,
        on_delete=models.CASCADE,
        related_name='rollcalls',
        verbose_name='درس ارائه شده'
    )
    rollcall = models.BooleanField(default=False, verbose_name='حاضر')
    rollcall_number = models.ForeignKey(to=RollCallTimePosition, on_delete=models.CASCADE, related_name='rollcalls', verbose_name='شماره حاضری')



    class Meta:
        verbose_name = 'حضور/غیاب'
        verbose_name_plural = 'حضور/غیاب ها'
        
    def __str__(self):
        return f"{self.session} - {self.week} - {self.student} - {self.professor} - {self.lesson}"


