from django.contrib import admin
from . import models


@admin.register(models.RollCallTimePosition)
class RollCallTimePositionAdmin(admin.ModelAdmin):
    list_display = ('number', 'time', 'position',)


@admin.register(models.RollCall)
class RollCallAdmin(admin.ModelAdmin):
    list_display = ('presented_course', 'student', 'professor', 'week', 'session', 'rollcall_number', 'rollcall', )